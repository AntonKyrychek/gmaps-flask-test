to start app you will need docker, docker compose, and python 3.7

after creating venv
```
docker-compose up -d
pip install -r requirements.txt
python3.7 run.py
```