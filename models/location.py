from extensions import db
from models.abstract_models import LocalDbModel


class Location(LocalDbModel):
    def __init__(self, **kwargs):
        super(Location, self).__init__(**kwargs)

    __tablename__ = "Locations"
    id = db.Column("id", db.Integer, primary_key=True)
    longitude = db.Column("longitude", db.FLOAT())
    latitude = db.Column("latitude", db.FLOAT())
    address = db.Column("address", db.String(255))
    label = db.Column("label", db.String(255))

    def as_dict(self):
        return {
            "id": self.id,
            "longitude": self.longitude,
            "latitude": self.latitude,
            "address": self.address,
            "label": self.label,
        }

    @classmethod
    def get_by_label(cls, label):
        return cls.query.filter(cls.label == label).first()