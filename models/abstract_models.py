from datetime import datetime

from extensions import db


class CreatedUpdatedMixin:
    created_on = db.Column(db.TIMESTAMP, default=datetime.utcnow())
    updated_on = db.Column(
        db.TIMESTAMP,
        default=datetime.utcnow(),
        onupdate=datetime.utcnow())


class GetActiveMixin:

    @classmethod
    def get_list_all(cls, active=None):

        if active is None:
            return cls.query.all()

        else:
            return cls.query.filter(
                cls.active == active).all()

    @classmethod
    def get_active(cls):
        return cls.query.filter(
            cls.active == True).all()


class LocalDbModel(db.Model):
    __abstract__ = True

    def save(self):
        """
        Save a model instance.

        :return: Model instance
        """
        db.session.add(self)
        db.session.commit()

        return self

    def delete(self):
        """
        Delete a model instance.

        :return: db.session.commit()'s result
        """
        db.session.delete(self)
        return db.session.commit()

    def __str__(self):
        """
        Create a human readable version of a class instance.

        :return: self
        """
        obj_id = hex(id(self))
        columns = self.__table__.c.keys()

        values = ', '.join("%s=%r" % (n, getattr(self, n)) for n in columns)
        return '<%s %s(%s)>' % (obj_id, self.__class__.__name__, values)

    @classmethod
    def get(cls, id):
        return cls.query.filter(cls.id == id).first()

    @classmethod
    def get_list_all(cls):
        return cls.query.all()

    @classmethod
    def get_filtered(cls, *args):
        """
        :param agrs:  cls.Column == Value
        :return:
        """
        return cls.query.filter(*args).all()

    @classmethod
    def get_len(cls, *args, **kwargs):
        """
                :param agrs:  cls.Column == Value
                :return:
                """
        # return cls.query(func.count(cls.id)).scalar()
        q = cls.query
        if args:
            q = q.filter(*args)
        return q.count()

    @classmethod
    def get_list(cls, filtered_by=None, ordered_by=None, limit=10, offset=None):
        """

        :param filtered_by: cls.Column == Value or list of these
        :param ordered_by: has to be with desc() or asc()
        :param limit:
        :param offset:
        :return:
        """
        q = cls.query
        if filtered_by:
            if isinstance(filtered_by, list):
                q = q.filter_by(*filtered_by)
            else:
                q = q.filter_by(filtered_by)
        if ordered_by:
            q = q.order_by(ordered_by)
        if limit:
            q = q.limit(limit)
        if offset:
            q = q.offset(offset)
        return q.all()

    def as_dict(self):
        return {x: getattr(self, x) for x in self.__table__.c.keys()}
