from os import environ
# try:
#     from envparse import env
#     from os.path import isfile
#     if isfile('.env'):
#         env.read_envfile('.env')
# except BaseException:
#     pass


DEBUG = False if environ.get('ENVIRONMENT_MODE') == 'production' else True

SECRET_KEY = "anton"

DB_HOST = environ.get('DB_HOST') or 'localhost'
DB_PORT = environ.get('DB_PORT') or '3306'
DB_USER = environ.get('DB_USER') or 'user'
DB_PASSWORD = environ.get('DB_PASSWORD') or 'pswd'
DEFAULT_DB = environ.get('DEFAULT_DB') or 'test'

SQLALCHEMY_TRACK_MODIFICATIONS = True


SQLALCHEMY_DATABASE_URI = f'mysql+pymysql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DEFAULT_DB}'


GOOGLEMAPS_KEY = environ.get('GOOGLEMAPS_KEY') or "PLEASE INSERT YOOU GMAPS KEY HERE"


TRIAL_PLAN = {
    "txs": 20,
    "reporting": False,
    "portfolios": 1,
    "tx_providers": 1,
    "editor_users": 1,
}
