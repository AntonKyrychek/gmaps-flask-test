FROM python:3

COPY . /app

#COPY deployment/nginx.conf /etc/nginx/
#COPY deployment/flask-site-nginx.conf /etc/nginx/conf.d/
#COPY deployment/uwsgi.ini /etc/uwsgi/
#COPY deployment/supervisord.conf /etc/

EXPOSE 4010

WORKDIR /app

run pip install -r requirements.txt
run chmod 777 -R logs

CMD ["python", "run.py"]
