from flask import Blueprint, Response, render_template, current_app

from controller import location_controller

location_router = Blueprint('location_router', __name__)


@location_router.route("/")
def route_app():
    ls = location_controller.get_all_locations()
    locations = [location.as_dict() for location in ls]
    return render_template('test.html',
                       # mymap=mymap,
                       locations=locations,
                       GMAPS_SECRET=current_app.config['GOOGLEMAPS_KEY'],
                       )


@location_router.route('/location', methods=["POST"])
def route_add_location():
    return Response(location_controller.location_save())


@location_router.route('/location', methods=["DELETE"])
def route_delete_location():
    return Response(location_controller.delete_location())


@location_router.route('/locations', methods=["DELETE"])
def route_delete_locations():
    return Response(location_controller.delete_locations())
