from werkzeug.exceptions import NotFound, Forbidden


class AccessDeniedException(Forbidden):
    description = "Access Denied"


class NoLocation(NotFound):
    description = "Marker/location Not Found"
