from flask import url_for


class TestPage(object):
    labels = []

    def test_get_app(self, client):
        response = client.get(url_for("location_router.route_app"))
        assert response.status_code == 200

    def test_add_location(self, client):
        label = 100
        response = self.create_location(label, client)
        self.labels.append(label)
        assert response.status_code == 200

    def test_delete_location(self, client):
        response = client.delete(url_for("location_router.route_delete_location"), query_string={"label": self.labels[0]})
        assert response.status_code == 200

    def test_delete_location_exeption(self, client):
        response = client.delete(url_for("location_router.route_delete_location"), query_string={"label": self.labels[0]})
        assert response.status_code == 404

    def create_location(self, label, client):
        data =  {
            "lat": 1.123,
            "lng": 1.121233,
            "address": "St. monica. soem street and w/e",
            "label": label,
        }
        return client.post(url_for("location_router.route_add_location"), data=data)

    def test_delete_location_all(self, client):
        list_labels = ["1", "2", "3", "4", "5", ]
        for label in list_labels:
            self.create_location(label, client)
        response = client.delete(url_for("location_router.route_delete_locations"),
                                 query_string={"labels": ",".join(list_labels)})
        assert response.status_code == 200

    def test_delete_location_all_exeption(self, client):
        list_labels = ["1", "2", "3", "4", "5", ]
        response = client.delete(url_for("location_router.route_delete_locations"),
                                 query_string={"labels": ",".join(list_labels)})
        assert response.status_code == 404
