from logging.config import dictConfig

from flask import Flask, send_from_directory
from flask.logging import default_handler
from werkzeug.exceptions import HTTPException

from config.settings_logging import LOGGING
from extensions import db
from router.map_router import location_router

dictConfig(LOGGING)


def create_app(settings_override=None):
    app = Flask(__name__, instance_relative_config=True, static_url_path='')
    app.config.from_object('config.settings')
    # app.config.from_pyfile('prepopulate.py', silent=True)
    print(app.config["SQLALCHEMY_DATABASE_URI"])
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

    if settings_override:
        app.config.update(settings_override)

    app.logger.removeHandler(default_handler)

    app.register_blueprint(location_router)
    extensions(app)

    @app.route('/static/<path:path>')
    def send_js(path):
        return send_from_directory('static', path)

    @app.errorhandler(Exception)
    def handle_error(e):
        from flask import jsonify
        code = 500
        if isinstance(e, HTTPException):
            code = e.code
        return jsonify(error=str(e)), code

    return app


def extensions(app):
    """
    Register 0 or more extensions (mutates the app passed in).

    :param app: Flask application instance
    :return: None
    """
    db.init_app(app)
    db.create_all(app=app)

    return None


if __name__ == "__main__":
    create_app().run(host='0.0.0.0', port=4010)
