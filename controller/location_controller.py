from flask import request

from exceptions.app_exceptions import NoLocation
from models.location import Location


def location_save():
    params = {
        "longitude": request.form['lng'],
        "latitude": request.form['lat'],
        "address": request.form['address'],
        "label": request.form['label'],
    }
    loc = Location(**params)
    loc.save()
    return "ok"


def get_all_locations():
    locations = Location.get_list(limit=0)
    return locations


def delete_location():
    label = request.args["label"]
    location = Location.get_by_label(label)
    if location:
        location.delete()
    else:
        raise NoLocation
    return "ok"


def delete_locations():
    labels = request.args["labels"].split(",")
    for label in labels:
        location = Location.get_by_label(label)
        if location:
            location.delete()
        else:
            raise NoLocation
    return "ok"
